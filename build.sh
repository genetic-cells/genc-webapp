#!/bin/bash
rm -r ./target/*
wasm-pack build --release --target web --out-name wasm --out-dir ./static
cp -r ./static/* ./target

#if [ -r "$0" ]
#then
#    echo "Starting..."
#    cd ./target
#    python3 -m http.server 8000 &
#    xdg-open http://0.0.0.0:8000/index.html
#fi
