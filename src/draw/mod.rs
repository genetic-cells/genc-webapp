mod drawer2d;

use genc_lib::entity::Entity;
use genc_lib::pos::Pos;
use genc_lib::world::{CellWorld, WCell};
use lazy_static::*;
use wasm_bindgen::__rt::core::mem::MaybeUninit;
use wasm_bindgen::{JsCast, JsValue};
use web_sys::*;

pub enum DrawError {
    Get2DContext,
}

#[derive(Copy, Clone)]
pub struct DrawMode {
    terrain: TerrainDrawMode,
    entity: EntityDrawMode,
}

#[derive(Copy, Clone)]
pub enum TerrainDrawMode {
    Nothing,
    Light,
    Food,
}

#[derive(Copy, Clone)]
pub enum EntityDrawMode {
    Nothing,
    Hash,
    En,
}

pub struct WDrawer {
    pub draw_mode: DrawMode,
    pub light_pallet: Box<[JsValue; 256]>,
    pub en_pallet: Box<[JsValue; 256]>,
}

impl WDrawer {
    // Получает цвет для рисования на данной ячейки из мира
    fn terrain_color(&self, c: &WCell) -> Option<&JsValue> {
        use TerrainDrawMode::*;
        match self.draw_mode.terrain {
            Nothing => None,
            Light => Some(&self.light_pallet[c.light as usize]),
            Food => unimplemented!(),
        }
    }

    // Получает цвет для рисования на данной сущности из мира
    fn entity_color(&self, e: Option<&Entity>) -> Option<&JsValue> {
        use EntityDrawMode::*;
        match self.draw_mode.entity {
            Nothing => None,
            En => Some(&self.en_pallet[e?.en as usize]),
            Hash => unimplemented!(),
        }
    }

    fn color_on(&self, w: &CellWorld, p: Pos) -> Option<&JsValue> {
        let c = &w.terrain()[p];
        let e = w.entities().get(p);
        self.entity_color(e).or(self.terrain_color(c))
    }

    pub fn draw(
        &self,
        canvas: &HtmlCanvasElement,
        w: &CellWorld,
        scale: usize,
    ) -> Result<(), DrawError> {
        let ctx: web_sys::CanvasRenderingContext2d = canvas
            .get_context("2d")
            .map_err(|_| DrawError::Get2DContext)?
            .ok_or(DrawError::Get2DContext)?
            .dyn_into::<web_sys::CanvasRenderingContext2d>()
            .map_err(|_| DrawError::Get2DContext)?;

        let width: f64 = canvas.width().into();
        let height: f64 = canvas.height().into();
        let scale = scale as f64;

        ctx.begin_path();
        ctx.scale(scale, scale);

        //ctx.set_fill_style(&JsValue::from_str("#000"));
        ctx.clear_rect(0.0, 0.0, width, height);

        let draw_cell = |p: Pos| {
            let x = p.x as f64;
            let y = p.y as f64;
            let c = self.color_on(w, p);
            if let Some(c) = c {
                ctx.set_fill_style(c);
                ctx.fill_rect(x, y, 1.0, 1.0);
            }
        };

        for x in 0..w.width() as isize {
            for y in 0..w.height() as isize {
                let p = Pos::new(x, y);
                draw_cell(p);
            }
        }

        ctx.set_transform(1.0, 0.0, 0.0, 1.0, 0.0, 0.0);
        ctx.stroke();

        Ok(())
    }
}

impl Default for WDrawer {
    fn default() -> Self {
        let scale = 1;
        let draw_mode = DrawMode {
            terrain: TerrainDrawMode::Light,
            entity: EntityDrawMode::En,
        };
        let light_pallet = gen_palette(|i| [i; 3]);
        let en_pallet = gen_palette(|i| [i / 2 + 127, i / 3 + 64, (255 - i) / 2 + 127]);

        Self {
            draw_mode,
            light_pallet,
            en_pallet,
        }
    }
}

pub fn gen_palette(f: impl Fn(u8) -> [u8; 3]) -> Box<[JsValue; 256]> {
    let mut v: [MaybeUninit<JsValue>; 256] =
        unsafe { std::mem::MaybeUninit::uninit().assume_init() };
    for i in 0..=255u8 {
        let c = f(i);
        let c = JsValue::from_str(&format!("rgb({},{},{})", c[0], c[1], c[2]));
        unsafe {
            std::ptr::write(v[i as usize].as_mut_ptr(), c);
        }
    }
    let l = unsafe { std::mem::transmute(v) };
    Box::new(l)
}
