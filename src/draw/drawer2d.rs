// pub enum CreateError {
//     Get2DContext,
// }
//
// pub struct Drawer2d {
//     canvas: HtmlCanvasElement,
//     context: CanvasRenderingContext2d,
// }
//
// impl Drawer2d {
//     pub fn new(canvas: HtmlCanvasElement) -> Result<Self, CreateError> {
//         let context: web_sys::CanvasRenderingContext2d = canvas
//             .get_context("2d")
//             .map_err(|_| CreateError::Get2DContext)?
//             .ok_or(CreateError::Get2DContext)?
//             .dyn_into::<web_sys::CanvasRenderingContext2d>()
//             .map_err(|_| CreateError::Get2DContext)?;
//
//         Ok(Self { canvas, context })
//     }
// }
