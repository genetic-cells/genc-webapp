use crate::draw::{DrawMode, WDrawer};
use genc_lib::pos;
use genc_lib::pos::Pos;
use genc_lib::processor::CellProcessor;
use genc_lib::simulation::Simulation;
use genc_lib::world::CellWorld;
use log::*;
use rand::prelude::*;
use serde_derive::{Deserialize, Serialize};
use strum::IntoEnumIterator;
use strum_macros::{EnumIter, ToString};
use wasm_bindgen::__rt::core::time::Duration;
use wasm_bindgen::__rt::std::time::SystemTime;
use wasm_bindgen::{JsCast, JsValue};
use yew::format::Json;
use yew::prelude::*;
use yew::services::interval::IntervalTask;
use yew::services::storage::{Area, StorageService};
use yew::services::IntervalService;

const KEY: &str = "yew.todomvc.self";
const WORLD_CANVAS_ID: &str = "world";

fn rand_u64() -> u64 {
    fn gen_byte() -> u64 {
        let byte = js_sys::Math::random() * 256.0;
        (byte as u8) as u64
    };

    gen_byte() << 0
        | gen_byte() << 8
        | gen_byte() << 16
        | gen_byte() << 24
        | gen_byte() << 32
        | gen_byte() << 40
        | gen_byte() << 48
        | gen_byte() << 56
}

// -----
// State
// -----

pub struct State {
    scale: usize,
    simulation: Simulation,
    drawer: WDrawer,
}

impl State {
    pub fn new() -> Self {
        let mut world = CellWorld::new(100, 60);
        let t = world.terrain_mut();
        for x in 0..t.width() {
            for y in 0..t.height() {
                let h = t.height();

                let mut c = &mut t[Pos::new(x as isize, y as isize)];
                c.light = ((((h - y) as f64) / (h as f64)) * 255.0) as u8;
            }
        }

        let mut proc = CellProcessor::default();

        proc.add_post_handler(|w, s, r| {
            if w.population() == 0 {
                let c = w.width() * w.height() / 5;
                w.gen_population(r, c);
            }
        });

        let mut sim = Simulation::with_seed(world, proc, rand_u64());

        Self {
            simulation: sim,
            scale: 8,
            drawer: WDrawer::default(),
        }
    }
}

// ---
// Msg
// ---

pub enum Msg {
    Nope,
    Tick,
    Draw,
}

// ---
// App
// ---

pub struct App {
    link: ComponentLink<Self>,
    storage: StorageService,
    draw_timer: IntervalTask,
    time: f64,
    state: State,
}

impl App {
    pub fn new(link: ComponentLink<Self>) -> Self {
        let draw_duration = Duration::from_secs_f64(1.0 / 30.0);
        let draw_callback = link.callback(|_| Msg::Draw);

        Self {
            link,
            storage: StorageService::new(Area::Local).unwrap(),
            draw_timer: IntervalService::spawn(draw_duration, draw_callback),
            time: 0.0,
            state: State::new(),
        }
    }
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        App::new(link)
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        self.time += 0.01;
        match msg {
            Msg::Draw => {
                if self.state.simulation.ticks() % 1 != 0 {
                    self.state.simulation.tick();
                    return true;
                }

                use web_sys::*;

                let document = web_sys::window().unwrap().document().unwrap();
                let canvas = document.get_element_by_id(WORLD_CANVAS_ID).unwrap();
                let canvas: web_sys::HtmlCanvasElement =
                    canvas.dyn_into::<web_sys::HtmlCanvasElement>().unwrap();

                self.state.scale = 8;
                self.state
                    .drawer
                    .draw(&canvas, self.state.simulation.world(), self.state.scale);

                self.state.simulation.tick();
            }
            Msg::Nope => {}
            Msg::Tick => {}
        }
        //self.storage.store(KEY, Json(&self.state.entries));
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        let ww = self.state.simulation.world().width();
        let wh = self.state.simulation.world().height();
        let scale = self.state.scale;

        return html! {
            <div class="wrapper">
                <section class="genc-app">
                    <span>{ format!("Seed: {}", self.state.simulation.seed()) }</span><br/>
                    <span>{ format!("Iteration: {}", self.state.simulation.ticks()) }</span><br/>
                    <span>{ format!("Population: {}", self.state.simulation.world().population()) }</span><br/>
                    { create_world_canvas(ww, wh, scale) }
                </section>
                <footer class="info">
                    <p>{ "Written by " }<a href="https://gitlab.com/VAVUS7/" target="_blank">{ "Vladislav Podporkin" }</a></p>
                </footer>
            </div>
        };
    }
}

fn create_world_canvas(w: usize, h: usize, scale: usize) -> Html {
    let sw = w * scale;
    let sh = h * scale;

    let style = format!("width={sw}px; height={sh}px", sw = sw, sh = sh);

    html! {
        <canvas id=WORLD_CANVAS_ID width={sw} height={sh} style={style}> </canvas>
    }
}
